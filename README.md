django-related-admin
====================

Show fields from related model in Django admin.

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/django-related-admin>
* GitHub: <https://github.com/sikaondrej/django-related-admin>


Documentation
-------------

### Instalation
Instalation is very simple over pip.

    pip install django-related-admin

### Fields
`db_fields.OneToOneField(RelatedModel)`

### Usage
add to settings.py

    INSTALLED_APPS += ("related_admin", )

and to root urls.py

    urlpatterns += patterns('', url(r'^related_admin/', include('related_admin.urls')))

In models

    from django.db import models
    from related_admin import db_fields as ra

    class MyType(models.Model):
        email = models.CharField(max_length=255)
        phone = models.CharField(max_length=255)

    class Person(models.Model):
        name = models.CharField(max_length=255)
        contact = ra.OneToOneField(MyType)