# -*- coding: utf-8 -*-
from django import forms
from django.db.models import get_model

import widgets

class RelatedOneField(forms.ModelChoiceField):
    def __init__(self, related_model, **kwargs):
        defaults = {
            'widget': widgets.RelatedOneWidget(related_model),
        }
        defaults.update(kwargs)
        super(RelatedOneField, self).__init__(**defaults)