# -*- coding: utf-8 -*-
from django.forms.widgets import Widget
from django.template import loader
from django.utils.safestring import mark_safe
from django.conf import settings
from django import forms

class RelatedOneWidget(Widget):
    def __init__(self, related, **kwargs):
        self.related = related
        return super(RelatedOneWidget, self).__init__(**kwargs)

    def render(self, name, value, attrs=None):
        return mark_safe(loader.render_to_string("related_admin/one/widget.html", {
            "STATIC_URL": settings.STATIC_URL,
            "instance_pk": value,
            "name": name,
            "model_str": "%s.%s" % (self.related.__module__, self.related.__name__),
        }))