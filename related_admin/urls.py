from django.conf.urls import patterns, url, include

urlpatterns = patterns('related_admin.views',
    url(r'^one/save/(?P<name>[\w]+)/(?P<model_str>[\w._]+)/$', 'one_save'),
    url(r'^one/rm/(?P<name>[\w]+)/(?P<model_str>[\w._]+)/$', 'one_rm'),
)