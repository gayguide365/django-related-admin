# -*- coding: utf-8 -*-
from django.db import models

import form_fields

class OneToOneField(models.OneToOneField):
    def __init__(self, to, **kwargs):
        self.related_model = to
        super(OneToOneField, self).__init__(to, **kwargs)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': form_fields.RelatedOneField,
            "related_model": self.related_model,
        }
        defaults.update(kwargs)
        return super(OneToOneField, self).formfield(**defaults)

    def south_field_triple(self):
        from south.modelsinspector import introspector
        field_class = "django.db.models.OneToOneField"
        args, kwargs = introspector(self)
        return field_class, args, kwargs
