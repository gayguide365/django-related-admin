# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse
from django.template.defaultfilters import slugify
from django.shortcuts import render_to_response
from django.conf import settings
from django import forms

from .helpers import get_model

def one_save(request, name, model_str):
    Model = get_model(model_str)
    class Form(forms.ModelForm):
        class Meta:
            model = Model

    instance = None
    pk_str = request.GET.get("pk", None)
    if pk_str and pk_str != "None":
        pk = int(pk_str)
        instance = Model.objects.get(pk=pk)

    form = Form(request.POST or None, prefix="related_%s" % name, instance=instance)
    if form.is_valid():
        instance = form.save()
    if instance:
        pk = instance.pk
    else:
        pk = None
    return HttpResponse(json.dumps({
        "form": form.as_p(), 
        "pk": pk, 
    }))

def one_rm(request, name, model_str):
    Model = get_model(model_str)

    pk_str = request.GET.get("pk", None)
    if pk_str and pk_str != "None":
        pk = int(pk_str)
        Model.objects.get(pk=pk).delete()

    return HttpResponse("")