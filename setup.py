#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-related-admin",
    version = "1.0.0",
    url = 'https://github.com/sikaondrej/django-related-admin/',
    license = 'MIT',
    description = "Show fields from related model in Django admin.",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = find_packages(),
    requires = [],
    include_package_data = True,
    zip_safe = False,
)
